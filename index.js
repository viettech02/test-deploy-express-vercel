const express = require("express");
const {product, v1} = require("./src/routers/product");

const PORT = process.env.PORT || 5050;

const app = express();
app.use(express.json());

// const v1 = require("./routers/v1");
// app.use("/api/v1", v1);

//test deploy respone
app.use("/test", (req, res, next) => { res.status(200).json('TESTTT  OooKkkkk') });
app.use("/test22", (req, res, next) => { res.status(200).json('TESTTT222 OooKkkkk') });

app.use("/api", product)
app.use("/api", v1)

app.use("/", (req, res, next) => { res.status(200).json('Deployyy OooKkkkk') });

app.listen(PORT, () => console.log(`Server is running in ${PORT} Okkkkk`));


module.exports = app;